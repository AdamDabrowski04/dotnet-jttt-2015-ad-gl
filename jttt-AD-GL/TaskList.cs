﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jttt_AD_GL
{
    class TaskList
    {
            
        public BindingList<Task> task_list;
        
        public TaskList()
        {
            task_list= new BindingList<Task>();
        }
        public void AddTask(string URL, string description, string email, string task_name)
        {
            task_list.Add(new Task(URL, description, email, task_name));
        }
        public void ProcessAllTasks()
        {
          foreach(var element in task_list)
          {
              element.DownloadPictureAndSendEmail();
          }           
        }

    }
}
