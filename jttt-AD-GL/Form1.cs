﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace jttt_AD_GL
{
    public partial class manageTasks : Form
    {

        BindingList<Task> task_list;
        public manageTasks()
        {
            InitializeComponent();

            task_list = new BindingList<Task>();
            listBox1.DataSource = task_list;
            using (var ctx = new BazaDbContext())
            {
                foreach (var a in ctx.Task)
                {
                    task_list.Add(a);
                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var task = new Task(URL.Text, description.Text, email.Text, task_name.Text);
            task_list.Add(task);
            using (var ctx = new BazaDbContext())
            {
                ctx.Task.Add(task);
                ctx.SaveChanges();
            }
        }

        private void getURL_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            bool oneOrMoreFailed=false;
            string tmp="Nie udalo sie zrealizowac nastepujacych zadan:\n";
            foreach (var element in task_list)
            {
                if(element.DownloadPictureAndSendEmail().Length>15) //Zwraca wiadomość na temat powodzenia operacji. Długość wiaodmości  dla udanej operacji <15
                {
                    oneOrMoreFailed = true;
                    tmp+= element.ToString() + Environment.NewLine;
                    using (StreamWriter file = File.AppendText("log.txt"))
                    {
                        file.WriteLine(DateTime.Now + " faild to realize: "+element.ToString());
                    }
                }
            }
            task_list.Clear();
            // Czy po wykonaniu zadania mają być usuwane z bazy danych?
            //using (var ctx = new BazaDbContext())
            //{
            //    foreach(var a in ctx.Task)
            //    {
            //        ctx.Task.Remove(a);
            //    }
            //}
            if(oneOrMoreFailed)
                MessageBox.Show(tmp);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void delete_Click(object sender, EventArgs e)
        {
            task_list.Clear();
            using (var ctx = new BazaDbContext())
            {
                foreach(var a in ctx.Task)
                {
                    ctx.Task.Remove(a);
                }
                ctx.SaveChanges();
            }
            
        }

        private void serialize_Click(object sender, EventArgs e)
        {
            FileStream fs = new FileStream("DataFile.dat", FileMode.Create);

            // Construct a BinaryFormatter and use it to serialize the data to the stream.
            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, task_list);
            }
            catch (SerializationException exception)
            {
                Console.WriteLine("Failed to serialize. Reason: " + exception.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }
        }

        private void deserialize_Click(object sender, EventArgs e)
        {
            FileStream fs = new FileStream("DataFile.dat", FileMode.Open);
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                BindingList<Task> task_list_deserialized = (BindingList<Task>)formatter.Deserialize(fs);
                foreach (var element in task_list_deserialized)
                {
                    task_list.Add(element);
                }
            }
            catch (SerializationException exception)
            {
                Console.WriteLine("Failed to deserialize. Reason: " + exception.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }
        }

        private void remove_task_Click(object sender, EventArgs e)
        {    
            var Index = listBox1.SelectedIndex;
            Task task = task_list.ElementAt(Index);
            task_list.Remove(task);
            using(var ctx = new BazaDbContext())
            {
                ctx.Task.Attach(task);
                ctx.Task.Remove(task);
                ctx.SaveChanges();
            }
        }

    }
}
