﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.IO;
namespace jttt_AD_GL
{
    [Serializable]
   public class SendEmail
    {
        string acces_path_;

        public SendEmail(string acces_path)
        {
            acces_path_ = acces_path;

        }
       public void SendEmailWithPicture(string email)
        {
            

            var fromAddress = new MailAddress("netjawatest@gmail.com", "AD_GL");
            var toAddress = new MailAddress(email, "AdamDąbrowski");
            const string fromPassword = "123net123";
            const string subject = "Funny picture";
            const string body = "Message sent automaticaly.";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };

            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                message.Attachments.Add(new Attachment(acces_path_));
                smtp.Send(message);
                using (StreamWriter file = File.AppendText("log.txt"))
                {
                    file.WriteLine(DateTime.Now + " sent file:" + acces_path_ + " to " + toAddress + Environment.NewLine);
                }
            }
 
        }

    }
    
}
