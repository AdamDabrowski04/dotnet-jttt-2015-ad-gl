﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jttt_AD_GL
{
    [Serializable]
    public class Task
    {
        public int Id { get; set; }
        public string URL_ { get; set; }
        public string description_{ get; set;}
        public string email_{ get; set;}
        public string task_name_ { get; set; }
        public Task(string URL, string description, string email, string task_name)
        {
            URL_=URL;
            description_ = description;
            email_=email;
            task_name_ = task_name;

        }
        public Task() { }
        public string DownloadPictureAndSendEmail()
        {
            DownloadPicture img = new DownloadPicture(URL_, description_);
            string img_path_ = img.Download();
            if(img_path_.Length>0)
            {
                SendEmail postcard = new SendEmail(img_path_);
                postcard.SendEmailWithPicture(email_);
                return "Email sent.";
            }
            else
            {
                return "No image with such description.";
            }
        }



        public override string ToString()
        {
            return string.Format("Id: {0} Nazwa zadania: {1} Szukana tresc: {2} URL: {3} email: {4} ", Id , task_name_ , description_ , URL_ , email_);

        }
    }
}
