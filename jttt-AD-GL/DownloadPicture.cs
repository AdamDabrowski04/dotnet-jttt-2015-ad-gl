﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Net;
using System.IO;


namespace jttt_AD_GL
{
    [Serializable]
   public class DownloadPicture
    {
        private string url_;
        private string description_;

        public DownloadPicture(string url, string description)
        {
            if (url.Contains("http://"))
                this.url_ = url;
            else
                this.url_ = "http://" + url;
            this.description_ = description;
        }

        private string GetPageHtml()
        {
            using(WebClient wc = new WebClient())
            {
                byte[] data = wc.DownloadData(url_);
                string html = System.Net.WebUtility.HtmlDecode(Encoding.UTF8.GetString(data));
                //string html = wc.DownloadString(url);
                return html;
            }
        }
       public string Download()
       {
           HtmlDocument doc = new HtmlDocument();

           string pageHtml = GetPageHtml();
           doc.LoadHtml(pageHtml);
           var nodes = doc.DocumentNode.Descendants("img");
           
           foreach(var node in nodes)
           {
               string pictureDescryption = node.GetAttributeValue("alt", "");
               string download_path = node.GetAttributeValue("src", "");
               string file_name = download_path.Substring(download_path.LastIndexOf('/') + 1);
               if (pictureDescryption.Contains(description_) && download_path.Contains("http"))
               {

                   string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\funny_pictures\\";
                   System.IO.Directory.CreateDirectory(path);
                   string imgDestination=path + file_name;
                   
                 using(WebClient wc = new WebClient())
                 {
                     wc.DownloadFile(download_path,imgDestination ); 
                 }
                 using (StreamWriter file = File.AppendText("log.txt"))
                 {
                     file.WriteLine(DateTime.Now + " downloaded file:" + file_name + " from " + url_);
                 }
                 return imgDestination;
               }

           }
           return "";
       }
    }


}
