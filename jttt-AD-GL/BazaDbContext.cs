﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jttt_AD_GL
{
    public class BazaDbContext : DbContext
    {
        // Aby podac wlasna nazwe bazy danych, nalezy wywolac konstruktor bazowy z nazwą jako parametrem.
        public BazaDbContext()
            : base("Zadania")
        {
            // Użyj klasy StudiaDbInitializer do zainicjalizowania bazy danych.
            Database.SetInitializer<BazaDbContext>(new BazaDBInitializer());
        }

        public DbSet<Task> Task { get; set; }

    }
}
