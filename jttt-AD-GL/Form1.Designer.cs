﻿namespace jttt_AD_GL
{
    partial class manageTasks
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda wsparcia projektanta - nie należy modyfikować
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddToList = new System.Windows.Forms.Button();
            this.description = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.URL = new System.Windows.Forms.TextBox();
            this.email = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.ProcessTasks = new System.Windows.Forms.Button();
            this.task_name = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.delete = new System.Windows.Forms.Button();
            this.deserialize = new System.Windows.Forms.Button();
            this.serialize = new System.Windows.Forms.Button();
            this.remove_task = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AddToList
            // 
            this.AddToList.Location = new System.Drawing.Point(16, 402);
            this.AddToList.Name = "AddToList";
            this.AddToList.Size = new System.Drawing.Size(241, 51);
            this.AddToList.TabIndex = 0;
            this.AddToList.Text = "Dodaj do listy";
            this.AddToList.UseVisualStyleBackColor = true;
            this.AddToList.Click += new System.EventHandler(this.button1_Click);
            // 
            // description
            // 
            this.description.Location = new System.Drawing.Point(16, 51);
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(310, 20);
            this.description.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Szukana fraza";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Adres strony obrazkami";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 190);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Email";
            // 
            // URL
            // 
            this.URL.Location = new System.Drawing.Point(15, 119);
            this.URL.Name = "URL";
            this.URL.Size = new System.Drawing.Size(311, 20);
            this.URL.TabIndex = 5;
            // 
            // email
            // 
            this.email.Location = new System.Drawing.Point(15, 206);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(311, 20);
            this.email.TabIndex = 6;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(394, 52);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(579, 290);
            this.listBox1.TabIndex = 7;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // ProcessTasks
            // 
            this.ProcessTasks.Location = new System.Drawing.Point(566, 346);
            this.ProcessTasks.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ProcessTasks.Name = "ProcessTasks";
            this.ProcessTasks.Size = new System.Drawing.Size(125, 52);
            this.ProcessTasks.TabIndex = 8;
            this.ProcessTasks.Text = "Wykonaj!";
            this.ProcessTasks.UseVisualStyleBackColor = true;
            this.ProcessTasks.Click += new System.EventHandler(this.button2_Click);
            // 
            // task_name
            // 
            this.task_name.Location = new System.Drawing.Point(15, 374);
            this.task_name.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.task_name.Name = "task_name";
            this.task_name.Size = new System.Drawing.Size(311, 20);
            this.task_name.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 352);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Nazwa zadania";
            // 
            // delete
            // 
            this.delete.Location = new System.Drawing.Point(695, 346);
            this.delete.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(84, 52);
            this.delete.TabIndex = 11;
            this.delete.Text = "Czyść";
            this.delete.UseVisualStyleBackColor = true;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // deserialize
            // 
            this.deserialize.Location = new System.Drawing.Point(809, 346);
            this.deserialize.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.deserialize.Name = "deserialize";
            this.deserialize.Size = new System.Drawing.Size(75, 25);
            this.deserialize.TabIndex = 12;
            this.deserialize.Text = "DeSerialize";
            this.deserialize.UseVisualStyleBackColor = true;
            this.deserialize.Click += new System.EventHandler(this.deserialize_Click);
            // 
            // serialize
            // 
            this.serialize.Location = new System.Drawing.Point(809, 375);
            this.serialize.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.serialize.Name = "serialize";
            this.serialize.Size = new System.Drawing.Size(75, 23);
            this.serialize.TabIndex = 13;
            this.serialize.Text = "Serialize";
            this.serialize.UseVisualStyleBackColor = true;
            this.serialize.Click += new System.EventHandler(this.serialize_Click);
            // 
            // remove_task
            // 
            this.remove_task.Location = new System.Drawing.Point(412, 347);
            this.remove_task.Name = "remove_task";
            this.remove_task.Size = new System.Drawing.Size(121, 51);
            this.remove_task.TabIndex = 14;
            this.remove_task.Text = "Usuń";
            this.remove_task.UseVisualStyleBackColor = true;
            this.remove_task.Click += new System.EventHandler(this.remove_task_Click);
            // 
            // manageTasks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 557);
            this.Controls.Add(this.remove_task);
            this.Controls.Add(this.serialize);
            this.Controls.Add(this.deserialize);
            this.Controls.Add(this.delete);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.task_name);
            this.Controls.Add(this.ProcessTasks);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.email);
            this.Controls.Add(this.URL);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.description);
            this.Controls.Add(this.AddToList);
            this.Name = "manageTasks";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.getURL_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddToList;
        private System.Windows.Forms.TextBox description;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox URL;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button ProcessTasks;
        private System.Windows.Forms.TextBox task_name;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.Button deserialize;
        private System.Windows.Forms.Button serialize;
        private System.Windows.Forms.Button remove_task;
    }
}

